Jinja2==2.10
-r bots/requirements.txt
gspread==0.6.2
oauth2client==4.1.2
ansible-vault==1.1.1
